# sam-weather-app-cd

This project contains source code and supporting files for a serverless application that can be deployed with the SAM CLI.
It includes the following files and folders

FILES AND FOLDERS

- weather - (FOLDER) Code for the application's Lambda function (see hello_world folder)
- events - (FOLDER) Invocation events that you can use to invoke the function
- tests - (FOLDER) Unit tests for the application code
- template.yaml - Template that defines the application's AWS resources

The application uses several AWS resources, including Lambda functions and an API Gateway API.
These resources are defined in the 'template.yaml' file in this project.
The template can be updated to add AWS resources through the same deployment process that updates the application code.
Also use the AWS Toolkit plugin for Visual Studio Code.

# Deploy the weather application

S3 BUCKET

- cd-sam-weather-bucket

BUILD THE APPLICATION

- sam build

PACKAGE THE APP AND PREPARE IT FOR DEPLOYMENT

- sam package --output-template-file packaged.yaml --s3-bucket cd-sam-weather-bucket

DEPLOY THE APP

- sam deploy --template-file packaged.yaml --region us-east-1 --capabilities CAPABILITY_IAM --stack-name sam-weather-app-cd

RETRIEVE THE API GATEWAY ENDPOINT URL

- aws cloudformation describe-stacks --stack-name sam-weather-app-cd --query 'Stacks[].Outputs'

SAMPLE OUTPUT

- get-started
    https://a9l2eqsvsa.execute-api.us-east-1.amazonaws.com/Prod/get-started/

- get-location
    https://a9l2eqsvsa.execute-api.us-east-1.amazonaws.com/Prod/get-location/

- get-weather
    https://a9l2eqsvsa.execute-api.us-east-1.amazonaws.com/Prod/get-weather/

***********************

For most scenarios, we recommend that you create APIs by specifying this resource type as an event source of your AWS::Serverless::Function resource, as shown in the following example.

  Type: AWS::Serverless::Api
  Properties:
    StageName: prod
    DefinitionUri: swagger.yml
