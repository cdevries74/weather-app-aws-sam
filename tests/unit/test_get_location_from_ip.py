# UNIT TEST
# Tests getting location coordinates from IP address

import unittest
from get_location_from_ip import lambda_handler

class TestGetLocationFromIP(unittest.TestCase):

    def test_get_location_from_ip(self):
        mock_api_call = {
            'queryStringParameters': {
                'ip': '3.220.153.184'
            }
        }
        
        res = lambda_handler(mock_api_call, {})
        self.assertEqual(res['statusCode'], 200)

    def test_get_location_from_fake_ip(self):
        mock_api_call = {
            'queryStringParameters' : {
                'ip': '999.999.999.999'
            }
        }

        res = lambda_handler(mock_api_call, {})
        self.assertEqual(res['statusCode'], 400)

if __name__ == '__main__':
    unittest.main()