# UNIT TEST
# Tests API to obtain location based on user input

import unittest
from get_weather import lambda_handler

class TestGetWeather(unittest.TestCase):

    def test_get_coordinates(self):
        mock_api_call = {
            'queryStringParameters': {
                'longitude': '-73.26370',
                'latitude': '41.14120',
                'DARK_SKY_SECRET_KEY': 'fbcf15d3d872f2f66720690d60e26343'
            }
        }
        
        res = lambda_handler(mock_api_call, {})
        self.assertEqual(res['statusCode'], 200)

    def test_fail_to_get_coordinates(self):
        mock_api_call = {
            'queryStringParameters': {
                'longitude': '-73.26370',
                'DARK_SKY_SECRET_KEY': 'fbcf15d3d872f2f66720690d60e26343'
            }
        }
        
        res = lambda_handler(mock_api_call, {})
        self.assertEqual(res['statusCode'], 400)

if __name__ == '__main__':
    unittest.main()